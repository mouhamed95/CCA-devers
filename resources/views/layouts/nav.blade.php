<!-- banner -->
<div class="banner">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="navbar-header navbar-left">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h1><a class="navbar-brand" href="#">Carre<span>four</span></a></h1>
                <h1><a class="navbar-brand" href="#">des Cult<span>ures</span></a></h1><br>
                <h1><a class="navbar-brand" href="#">et des <span>Arts</span></a></h1>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                <nav class="menu menu--iris">
                    <ul class="nav navbar-nav menu__list">
                        <li class="menu__item menu__item--current"><a href="{{ url('/') }}" class="menu__link">Acceuil</a></li>
                        <li class="menu__item"><a href="music.html" class="menu__link">CCA</a></li>
                        <li class="menu__item"><a href="gallerie" class="menu__link">Gallerie</a></li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown">Musique<b class="caret"></b></a>
                            <ul class="dropdown-menu agile_short_dropdown">
                                <li><a href="icons.html">Web Icons</a></li>
                                <li><a href="typography.html">Typography</a></li>
                            </ul>
                        </li>
                        <li class="dropdown menu__item">
                            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown">Danse<b class="caret"></b></a>
                            <ul class="dropdown-menu agile_short_dropdown">
                                <li><a href="icons.html">Web Icons</a></li>
                                <li><a href="typography.html">Typography</a></li>
                            </ul>
                        </li>
                        <li class="menu__item"><a href="mail.html" class="menu__link">A propos</a></li>
                    </ul>
                </nav>
            </div>
        </nav>
        <div class="agile_banner_info">
            <!-- <h3>music</h3>
            <div class="agile_banner_info_pos">
                <h2>w3layouts</h2>
            </div> -->
        </div>
    </div>
</div>
<!-- //banner -->